const express = require('express');
const http = require('http');
const cors = require('cors');
const app = express();
const server = http.Server(app);
const texts = require('./data');

  let rooms = [];
  const users = [];

  const io = require('socket.io')(server, {
    cors: {
      origin: '*'
    }
  });
  
  const registerRoomHandlers = require('./socket/room');
const getRandomInt = require('./utils/getRandomInt');

  const onConnection = (socket) => {
    const { username } = socket.handshake.query;
    users.push({
      name: username,
      isReady: false
    });
    socket.username = username;
  
    registerRoomHandlers(io, socket, rooms, users);
  
    socket.on('disconnect', () => {
      // console.log(rooms);
      // socket.leave(roomId);
    });
  };
  
  io.on('connection', onConnection);
  
  app.use(cors());
  
  app.get('/rooms', (req, res) => {
    res.json(rooms);
  });
  
  app.get('/rooms/:name', (req, res) => {
    const { name } = req.params;
    const room = rooms.find(r => r.name === name);
  
    res.json(room);
  });
  
  const PORT = process.env.PORT || 5000;
  server.listen(PORT, () => {
    console.log(`Server ready. Port: ${PORT}`);
  });
