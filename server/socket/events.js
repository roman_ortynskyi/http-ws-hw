module.exports = {
    CREATE_ROOM: 'CREATE_ROOM',
    ROOM_CREATED: 'ROOM_CREATED',
    JOIN_ROOM: 'JOIN_ROOM',
    USER_JOINED_ROOM: 'USER_JOINED_ROOM',
    CHANGE_USER_IS_READY: 'CHANGE_USER_IS_READY',
    USER_IS_READY_CHANGED: 'USER_IS_READY_CHANGED',
    TIMER_BEFORE_GAME_TICK: 'TIMER_BEFORE_GAME_TICK',
    GAME_STARTED: 'GAME_STARTED',
    GAME_TIMER_TICK: 'GAME_TIMER_TICK',
    UPDATE_USER_PROGRESS: 'UPDATE_USER_PROGRESS',
    USER_PROGRESS_UPDATED: 'USER_PROGRESS_UPDATED',
    GAME_OVER: 'GAME_OVER'
};