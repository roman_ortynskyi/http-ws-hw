const { MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME } = require("../config");
const texts = require("../data");
const getRandomInt = require("../utils/getRandomInt");
const events = require("./events")

module.exports = (io, socket, rooms, users) => {

  socket.on(events.CREATE_ROOM, (roomName) => {
    socket.join(roomName);
    const { username } = socket;
    const user = {
      name: username,
      isReady: false,
      progress: 0
    }

    const room = {
      name: roomName,
      users: [user]
    };
    rooms.push(room);

    io.emit(events.ROOM_CREATED, room);
  });

  socket.on(events.JOIN_ROOM, (roomName) => {
    socket.join(roomName);
    const roomIndex = rooms.findIndex(r => r.name === roomName);
    const room = rooms[roomIndex];
    const { username } = socket;
    const user = {
      name: username,
      isReady: false,
      progress: 0
    };
    const updatedRoom = {
      ...room,
      users: [...room.users, user]
    };
    rooms[roomIndex] = updatedRoom;

    socket.broadcast.emit(events.USER_JOINED_ROOM, {
      room: updatedRoom,
      user
    });
  })

  socket.on(events.CHANGE_USER_IS_READY, (isReady) => {
    const { username } = socket;
    
    const room = rooms.find(r => r.users.map(user => user.name).includes(username));

    const userIndex = room.users.findIndex(user => user.name === username);

    const user = room.users[userIndex];

    const updatedUser = {
      ...user,
      isReady
    };

    room.users[userIndex] = updatedUser;

    io.to(room.name).emit(events.USER_IS_READY_CHANGED, updatedUser);

    if(room.users.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
      let remainingSeconds = SECONDS_TIMER_BEFORE_START_GAME;
      const interval = setInterval(() => {
        io.to(room.name).emit(events.TIMER_BEFORE_GAME_TICK, remainingSeconds);
        remainingSeconds--;
      }, 1000)
      
      setTimeout(() => {
        clearInterval(interval);

        const textIndex = getRandomInt(0, texts.length);
        io.to(room.name).emit(events.GAME_STARTED, {
          text: texts[textIndex]
        });

        let gameTimerSeconds = SECONDS_FOR_GAME;
        let gameTimerInterval = setInterval(() => {
          io.to(room.name).emit(events.GAME_TIMER_TICK, gameTimerSeconds);
          gameTimerSeconds--;
        }, 1000); 

        setTimeout(() => {
          clearInterval(gameTimerInterval);
          const results = room.users.sort((a, b) => a.finished - b.finished);
          io.to(room.name).emit(events.GAME_OVER, results);
        }, (SECONDS_FOR_GAME + 1) * 1000);

      }, (SECONDS_TIMER_BEFORE_START_GAME + 1) * 1000);
    }
  })

  socket.on(events.UPDATE_USER_PROGRESS, progress => {
    const { username } = socket;
    const room = rooms.find(r => r.users.map(user => user.name).includes(username));

    const userIndex = room.users.findIndex(user => user.name === username);

    const user = room.users[userIndex];

    const updatedUser = {
      ...user,
      progress
    };

    room.users[userIndex] = updatedUser;

    io.to(room.name).emit(events.USER_PROGRESS_UPDATED,updatedUser);

    if(progress === 100) {
      room.users[userIndex] = {
        ...updatedUser,
        finished: Date.now()
      }
    }
  });
}
