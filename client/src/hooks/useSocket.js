import { useEffect, useRef, useState } from 'react'
import io from 'socket.io-client'
import { nanoid } from 'nanoid'
// hooks
import { useLocalStorage, useBeforeUnload } from 'hooks'

const SERVER_URL = 'http://localhost:5000'

export const useSocket = () => {
  const socketRef = useRef(null);

  const connect = (username) => {
    socketRef.current = io(SERVER_URL, {
      query: { username }
    });

    console.log(socketRef);
  };

  const disconnect = () => {
    socketRef.current.disconnect();
  }
    

  // const sendMessage = ({ messageText, senderName }) => {
  //   socketRef.current.emit('message:add', {
  //     userId,
  //     messageText,
  //     senderName
  //   })
  // }

  // const removeMessage = (id) => {
  //   socketRef.current.emit('message:remove', id)
  // }

  // useBeforeUnload(() => {
  //   socketRef.current.emit('user:leave', userId)
  // })

  return { socketRef, connect, disconnect };
}
