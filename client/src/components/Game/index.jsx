import { useState, useRef } from 'react'
import { Link } from 'react-router-dom'
// hooks
import { useLocalStorage } from 'hooks'
// styles
import { Form, Button } from 'react-bootstrap'
import { useSocket } from 'hooks';
import { useContext } from 'react';
import { AppContext } from 'App';
import { useEffect } from 'react';
import RoomCard from 'components/RoomCard';
import { RoomsPage } from 'components/RoomsPage';
import { GamePage } from 'components/GamePage';

export function Game() {
  const { selectedRoom } = useContext(AppContext);

  return selectedRoom ? <GamePage /> : <RoomsPage />;
};
