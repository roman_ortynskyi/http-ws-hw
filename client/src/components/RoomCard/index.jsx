import React from 'react';

function RoomCard({
    name,
    users,
    onSelect
}) {
    return (
        <div class="room">
            {users.length} connected
            <h3>{name}</h3>
            <button onClick={onSelect}>Join</button>
        </div>
    );
};

export default RoomCard;
