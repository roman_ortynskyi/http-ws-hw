import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { useSocket } from '../../hooks/useSocket';
import { AppContext } from '../../App';

export function Login() {
    const [currentUsername, setCurrentUsername] = useState('');
    const history = useHistory();
    const { connect, setUsername } = useContext(AppContext);

    const onSubmit = () => {
        sessionStorage.setItem('username', currentUsername);
        setUsername(currentUsername);
        connect(currentUsername);
        history.push('/game');
        
    };  

    return (
        <div id="login-page" class="full-screen flex-centered">
            <div class="flex">
                <div class="username-input-container">
                    <input 
                        id="username-input" 
                        autofocus 
                        placeholder="username" 
                        type="text" 
                        value={currentUsername}
                        onChange={(e) => { setCurrentUsername(e.target.value) }}
                    />
                </div>
                <button onClick={onSubmit} id="submit-button" class="no-select">submit</button>
            </div>
         </div>
    );
};
