import { useState, useRef } from 'react'
import { useContext } from 'react';
import { AppContext } from 'App';
import { useEffect } from 'react';
import RoomCard from 'components/RoomCard';
import { MAXIMUM_USERS_FOR_ONE_ROOM } from 'config';

export function RoomsPage() {

  const [isGamePageOpen, setIsGamePageOpen] = useState(false);

  const { socketRef, rooms, setRooms, selectedRoom, setSelectedRoom, username } = useContext(AppContext);

  const createRoom = () => {
    const roomName = prompt();
    console.log(socketRef);
    socketRef.current.emit('CREATE_ROOM', roomName);
  }

  const onSelectRoom = room => {;
    setSelectedRoom(room);
    socketRef.current.emit('JOIN_ROOM', room.name);
  }

  useEffect(() => {
     (async () => {
      const url = 'http://localhost:5000/rooms';
      const response = await fetch(url, {
        method: 'GET'
      });
      const existingRooms = await response.json();

      const availableRooms = existingRooms
        .filter(room => room.users.length < MAXIMUM_USERS_FOR_ONE_ROOM);

      setRooms(availableRooms);
    })();
  }, []);

  useEffect(() => {
    socketRef.current.on('ROOM_CREATED', (room) => {
      // if created by me
      if(room.users.map(user => user.name).includes(username)) {
        setSelectedRoom(room);
      }
      setRooms((prevRooms) => [...prevRooms, room]);
    })

    return () => {
      socketRef.current.off('ROOM_CREATED');
    }
  }, []);

  useEffect(() => {
      socketRef.current.on('USER_JOINED_ROOM', async (data) => {
        const url = 'http://localhost:5000/rooms';
        const response = await fetch(url, {
          method: 'GET'
        });
        const existingRooms = await response.json();
      
        const availableRooms = existingRooms
            .filter(room => room.users.length < MAXIMUM_USERS_FOR_ONE_ROOM);

        setRooms(availableRooms);
      })

      return () => {
          socketRef.current.off('USER_JOINED_ROOM');
      }
  }, []);

  return (
    <div id="rooms-page" class="full-screen">
        <h1>Join Room Or Create New</h1>
        <button 
            id="add-room-btn"
            onClick={createRoom}
        >Create Room</button>
        {rooms.map(room => <RoomCard {...room} onSelect={() => { onSelectRoom(room) }}/>)}
    </div>
    );
};
