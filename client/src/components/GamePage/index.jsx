import { useState, useRef, useCallback } from 'react'
import { Link } from 'react-router-dom'
// hooks
import { useLocalStorage } from 'hooks'
// styles
import { Form, Button } from 'react-bootstrap'
import { useSocket } from 'hooks';
import { useContext } from 'react';
import { AppContext } from 'App';
import { useEffect } from 'react';
import RoomCard from 'components/RoomCard';
import { RoomsPage } from 'components/RoomsPage';
import { User } from '../User';
import { MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME } from 'config';

export function GamePage() {
  const { socketRef, selectedRoom, setSelectedRoom } = useContext(AppContext);

  const { name: roomName, users } = selectedRoom;

  const [seconds, setSeconds] = useState(SECONDS_TIMER_BEFORE_START_GAME);

  const [secondsVisible, setSecondsVisible] = useState(false);
  const [readyButtonVisible, setReadyButtonVisible] = useState(true);
  const [text, setText] = useState('');
  const [characterIndex, setCharacterIndex] = useState(0);

  useEffect(() => {
    socketRef.current.on('TIMER_BEFORE_GAME_TICK', (seconds) => {
      setSecondsVisible(true);
      setSeconds(seconds);
    });
  }, []);

  useEffect(() => {
    
    socketRef.current.on('GAME_STARTED', async (data) => {
      setText(data.text);
    });

    return () => {
      socketRef.current.off('GAME_STARTED');
      // document.removeEventListener('keypress');
    };
  }, [socketRef, text]);

  const handleKeyPress = useCallback((e) => {
    // if text was loaded
    if(text.length) {
        if(text.substring(characterIndex, characterIndex + 1) === e.key) {
          setCharacterIndex(characterIndex + 1);
        }
    }
  }, [characterIndex, text]);

  useEffect(() => {
    window.addEventListener('keypress', handleKeyPress);

    return () => {
      window.removeEventListener('keypress', handleKeyPress);
    };
  }, [handleKeyPress]);

  useEffect(() => {
    if(text.length > 0 && characterIndex > 0) {
      const progressPercentage = characterIndex / text.length * 100; 
      socketRef.current.emit('UPDATE_USER_PROGRESS', progressPercentage);
    }
  }, [socketRef, characterIndex, text]);

  useEffect(() => {
    socketRef.current.on('USER_PROGRESS_UPDATED', (user) => {
      const { name } = user;

      const userIndex = users.findIndex(u => u.name === name);

      const updatedUsers = [
        ...users.slice(0, userIndex),
        user,
        ...users.slice(userIndex + 1)
      ];

      setSelectedRoom({
        ...selectedRoom,
        users: updatedUsers
      });
    });

    return () => {
      socketRef.current.off('USER_JOINED_ROOM');
    }
  }, [socketRef]);

  useEffect(() => {
    socketRef.current.on('GAME_OVER', users => {
      const message = users.map((user, i) => `${i + 1} ${user.name}`).join('\n');
      alert(message);
    })
  }, [socketRef]);

  useEffect(() => {
    socketRef.current.on('GAME_TIMER_TICK', async (seconds) => {
      setSecondsVisible(true);
      setSeconds(seconds);
    });
  }, [socketRef]);

  useEffect(() => {
    (async () => {
      const response = await fetch(`http://localhost:5000/rooms/${roomName}`);
      const room = await response.json();

      setSelectedRoom(room);
    })();
  }, []);

  useEffect(() => {
    socketRef.current.on('USER_JOINED_ROOM', (data) => {
      setSelectedRoom(room => {
        return {
          ...room,
          users: [...room.users, data.user]
        }
      })
    })

    return () => {
      socketRef.current.off('USER_JOINED_ROOM');
    }
  }, []);

  useEffect(() => {
    socketRef.current.on('USER_IS_READY_CHANGED', (user) => {
      const { name } = user;

      const { users } = selectedRoom;

      const userIndex = users.findIndex(user => user.name === name);

      const updatedUsers = [
        ...users.slice(0, userIndex),
        user,
        ...users.slice(userIndex + 1)
      ];

      if(updatedUsers.every(user => user.isReady) && updatedUsers.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
        setReadyButtonVisible(false);
      }

      setSelectedRoom((currentSelectedRoom) => ({
        ...currentSelectedRoom,
        users: updatedUsers
      }))

    });

    return () => {
      socketRef.current.off('USER_IS_READY_CHANGED');
    }
  }, [selectedRoom, socketRef, setSelectedRoom])

  const [isReady, setIsReady] = useState(false);

  const readyButtonText = isReady ? 'Not Ready' : 'Ready';

  const onReadyClick = () => {
    socketRef.current.emit('CHANGE_USER_IS_READY', !isReady);
    setIsReady(!isReady);
  }

  const gamePage = (
    <div id="game-page" class="full-screen">
        <ul>
        {users?.map(user => <li key={user.name}>
            <User {...user}/>
        </li>)}
        </ul>
        {readyButtonVisible && <button onClick={onReadyClick}>{readyButtonText}</button>}
        {secondsVisible && <span>{seconds}</span>}
        <span style={{backgroundColor: 'green'}}>{text.substring(0, characterIndex)}</span>
        <span>{text.substring(characterIndex)}</span>
    </div>
  );

  return gamePage;
};
