import React from 'react'
import './style.css'

export function User({
    name,
    isReady,
    progress
}) {
    const color = isReady ? 'green' : 'red';

    return (
        <div className={`user-progress ${name}`}>
            <div className="top">
                <span className={`ready-status ready-status-${color}`}></span>
                <h3>{name}</h3>
            </div>
            <div className="bottom">
                <div 
                    className="progress" 
                    style={{
                        height: '100%',
                        width: `${progress}%`,
                        backgroundColor: 'green'
                    }}
                >    
                </div>
            </div>
        </div>
    );
};
