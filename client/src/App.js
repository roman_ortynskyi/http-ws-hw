import React, { useRef, createContext, useState, useEffect } from 'react';
import io from 'socket.io-client';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import './style.css'
// styles
import { Container } from 'react-bootstrap'
// components
import { Game, Login } from 'components'

const SERVER_URL = 'http://localhost:5000'

export const AppContext = createContext();

export const App = () => {
  const socketRef = useRef(null)
  
  const [rooms, setRooms] = useState([]);
  const [username, setUsername] = useState(null);
  const [selectedRoom, setSelectedRoom] = useState(null);

  const connect = (username) => {
    socketRef.current = io(SERVER_URL, {
      query: { username }
    });

    console.log(socketRef);

    
  };



  const disconnect = () => {
    socketRef.current.disconnect();
  }

  const appContextValue = { 
    socketRef, 
    connect, 
    disconnect, 
    rooms, 
    setRooms, 
    username, 
    setUsername,
    selectedRoom,
    setSelectedRoom
  };

  useEffect(() => {
    setUsername(sessionStorage.getItem('username'));
    connect(sessionStorage.getItem('username'));
  }, []);

  return (
    <AppContext.Provider value={appContextValue}>
      <Router>
        <Container style={{ maxWidth: '512px' }}>
          <Switch>
                <Route exact path={'/login'}>
                    {username ? <Redirect to={'/game'} /> : <Login />}
                </Route>
                <Route exact path={'/game'}>
                    {username ? <Game /> : <Redirect to={'/login'} />} 
                </Route>
                <Route path="*">
                    <Redirect to={'/login'} />
                </Route>
            </Switch>
        </Container>
      </Router>
    </AppContext.Provider>
  );
};